import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  const Calculator({super.key, required this.title});

  final String title;

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  int _counter = 0;
  int _increment = 0;
  int _count = 0;

  final _incrementController = TextEditingController();

  void _incrementCounter() {
    setState(() {
      _counter += _increment;
      _count += 1;
    });
  }

  void _updateIncrement(int value) {
    setState(() {
      _increment = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              controller: _incrementController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
            Text(
              'Vous avez cliqué $_count fois.',
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '$_counter + $_increment = ',
                ),
                Text(
                  '${_counter + _increment}',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromARGB(255, 219, 159, 230),
        onPressed: () {
          try {
            final int val = int.parse(_incrementController.text);

            _updateIncrement(val);
            _incrementCounter();
          } catch (e) {
            if (kDebugMode) {
              print(e);
            }
          }
        },
        tooltip: 'Increment',
        child: Text('+ $_increment'),
      ),
    );
  }
}
